# Soothing Baby with the 5Ss:
https://www.dropbox.com/s/4w9hd7pztdvd060/Soothing%20Baby%20-%20The%205Ss.MOV?dl=0

# Swaddling Baby - for use with the 5Ss:
https://www.dropbox.com/s/0l57qtz6e815xgi/Swaddling%20a%20Baby.MOV?dl=0

# Burping Techniques for a Newborn:
https://www.dropbox.com/s/3j3eksym4fbjdk9/Burping%20Techniques%20for%20a%20Newborn.MOV?dl=0
