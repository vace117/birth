<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1588838455251" ID="ID_1867234861" MODIFIED="1588840124212" STYLE="bubble" TEXT="Birth">
<node CREATED="1588840068215" ID="ID_1167235692" MODIFIED="1588927878343" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Prelabor </b>[10.85]
    </p>
  </body>
</html>
</richcontent>
<node COLOR="#990000" CREATED="1588841348643" ID="ID_596505087" MODIFIED="1588928882047" TEXT="Less Normal">
<node COLOR="#990000" CREATED="1588841402588" ID="ID_228945223" MODIFIED="1588928880732" TEXT="Water Breaks">
<node CREATED="1588841425748" ID="ID_790245321" MODIFIED="1588841432265" TEXT="Water leaks or gushes from the vagina">
<node CREATED="1588841462117" ID="ID_1189513380" MODIFIED="1588841487073" TEXT="&quot;Leak&quot; is a squirt that occurs when a pregnant person changes position">
<node CREATED="1588841496565" ID="ID_881035417" MODIFIED="1588841498602" TEXT="2 in 10 labors begin this way"/>
</node>
<node CREATED="1588841512732" ID="ID_1917784375" MODIFIED="1588841514057" TEXT="A &#x201c;gush&#x201d; is an uncontrollable heavy flow that may start with a popping feeling.">
<node CREATED="1588841525069" ID="ID_324195718" MODIFIED="1588841526281" TEXT="1 or 2 in 10 labors begins this way."/>
</node>
<node CREATED="1588841535285" ID="ID_992826152" MODIFIED="1588841582665" TEXT="The color of the fluid: Normally, the fluid is clear. If it is brownish or greenish, the baby may have had a bowel movement, which happens when a baby is stressed in the uterus. Such stress is caused by a temporary lack of oxygen.">
<node CREATED="1588841585246" ID="ID_1408208387" MODIFIED="1588841596115" TEXT="Usually not serious"/>
</node>
<node CREATED="1588841612773" ID="ID_940191732" MODIFIED="1588841615377" TEXT="The odor of the fluid. Normally, the fluid is practically odorless. If it has a foul smell, there may be an infection within the uterus, which could spread to the baby."/>
<node CREATED="1588841648782" ID="ID_966288824" MODIFIED="1588841671555" TEXT="More info: 10.80"/>
<node COLOR="#ff0000" CREATED="1588841709583" ID="ID_127196830" MODIFIED="1588841780975" TEXT="Possible EMERGENCY: Prolapsed cord [16.185]"/>
</node>
</node>
</node>
<node CREATED="1588840209689" ID="ID_401449417" MODIFIED="1588929284467" TEXT="Normal [11.433]">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1588840233833" ID="ID_1362820285" MODIFIED="1588840244845" TEXT="Starts between 37 - 42 weeks"/>
<node CREATED="1588840397082" ID="ID_895771751" MODIFIED="1588842003883">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Baby <b>&quot;Presentation&quot;</b>&#160;is Occiput Anterior (OA) [10.28]
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1588840818815" ID="ID_1272592285" MODIFIED="1588841135768" TEXT="Non-progressing contractions (Braxton Hicks)">
<node CREATED="1588840875791" ID="ID_1358677207" MODIFIED="1588840994624" TEXT="May come and go for a period of hours to days"/>
<node CREATED="1588841024304" ID="ID_394707813" MODIFIED="1588841029222" TEXT="Do not become longer, stronger, or closer together over time."/>
<node CREATED="1588844231974" ID="ID_253271211" MODIFIED="1588844251348" TEXT="May even stop after a few hours"/>
</node>
<node CREATED="1588840849791" ID="ID_679151246" MODIFIED="1588844295116" TEXT="The cervix softens, moves forward, and thins, but does not dilate beyond 1 to 2 cm"/>
<node CREATED="1588844643257" ID="ID_523237431" MODIFIED="1588844671312" TEXT="Stages [10.94]">
<node CREATED="1588841891369" ID="ID_280833335" MODIFIED="1588842075300">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>1) </b>The cervix softens (ripens)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1588842027873" ID="ID_634067174" MODIFIED="1588842140843">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>2) </b>The position of the cervix changes.
    </p>
  </body>
</html></richcontent>
<node CREATED="1588842054626" ID="ID_1290958152" MODIFIED="1588842056199" TEXT="posterior"/>
<node CREATED="1588842057018" ID="ID_672811776" MODIFIED="1588842062119" TEXT="midline"/>
<node CREATED="1588842062834" ID="ID_1953086208" MODIFIED="1588842065343" TEXT="anterior"/>
</node>
<node CREATED="1588842152403" ID="ID_1570744140" MODIFIED="1588842344222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>3)</b>&#160;The cervix thins and shortens (effaces)
    </p>
  </body>
</html></richcontent>
<node CREATED="1588842204539" ID="ID_1438669013" MODIFIED="1588844463087">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Starts at 3.5 cm long and gradually shortens and becomes paper-thin. The amount of thinning (effacement) is measured in two ways:
    </p>
  </body>
</html></richcontent>
<node CREATED="1588842241476" ID="ID_1035506030" MODIFIED="1588842273941">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Percentage: </b>Zero percent means no thinning or shortening has occurred; 50 percent means the cervix is about half its former thickness; 100 percent means it is paper-thin.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1588842278284" ID="ID_1589931720" MODIFIED="1588842322909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Centimeters of length: </b>One and one-half inches (3.5 cm) is the same as 0% effaced
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1588840105208" ID="ID_884982534" MODIFIED="1588840124212" POSITION="right" TEXT="Labor">
<node CREATED="1588841857608" ID="ID_441230650" MODIFIED="1588844664300" TEXT="Stages [10.94]">
<node CREATED="1588842345420" ID="ID_1925282457" MODIFIED="1588842383052">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>4)</b>&#160;The cervix opens (dilates)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588842432886" ID="ID_1636038175" MODIFIED="1588842438996" TEXT="Measured in centimeters. "/>
<node CREATED="1588842440166" ID="ID_1196631618" MODIFIED="1588842457706" TEXT="Occurs with progressing contractions"/>
<node CREATED="1588842463254" ID="ID_1074451167" MODIFIED="1588842508437">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Common for cervix to dilate 1 to 3 cm before Positive Signs of labor
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588842512254" ID="ID_959827864" MODIFIED="1588844508462" TEXT="Must eventually open to ~10 cm"/>
<node CREATED="1588844539913" ID="ID_1914918045" MODIFIED="1588926859309">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>First Stage</b>: Dilation [11.82]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588844993613" ID="ID_356727682" MODIFIED="1588845006502" TEXT="The change from prelabor to the dilation stage may be gradual, so don&#x2019;t expect that either of you will know the moment the cervix begins to open."/>
<node CREATED="1588844704939" ID="ID_449708727" MODIFIED="1588844733255">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dilation begins when the prelabor contractions change from their nonprogressing pattern to becoming <b>longer, stronger, and closer together</b>&#160;(or at least two of those three).
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1588844777123" ID="ID_1754064812" MODIFIED="1588844813234">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Can proceed very quickly, unevenly, or slowly.
    </p>
  </body>
</html></richcontent>
<node CREATED="1588845055102" ID="ID_304200446" MODIFIED="1588845061298" TEXT="2 - 24 hours"/>
<node CREATED="1588845070886" ID="ID_300339658" MODIFIED="1588845108965">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Early contractions may last from 30 to 40 seconds and come anywhere from 5 to 20 minutes apart.
    </p>
  </body>
</html></richcontent>
<node CREATED="1588845113390" ID="ID_1529322109" MODIFIED="1588845114130" TEXT="Usually painless."/>
</node>
<node CREATED="1588845119806" ID="ID_1087107754" MODIFIED="1588845130052" TEXT="As they progress in length, frequency, and duration, they become more intense and painful."/>
<node CREATED="1588845150134" ID="ID_919771505" MODIFIED="1588845166146" TEXT="By the time the cervix opens to 8 or 9 cm, the contractions may last 90 seconds or more, feel very intense (almost surely very painful), and come every 2 to 4 minutes."/>
</node>
<node CREATED="1588844815572" ID="ID_1591738164" MODIFIED="1588926961894">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Distinct phases
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588844834211" ID="ID_806347604" MODIFIED="1588845436037">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Latent phase</b>&#160;(or <b>Early Labor</b>) [11.96]
    </p>
  </body>
</html></richcontent>
<node CREATED="1588845315600" ID="ID_942312858" MODIFIED="1588845399386" TEXT="60-75% of the entire Dilation Stage"/>
<node CREATED="1588845224703" ID="ID_518195127" MODIFIED="1588845238461" TEXT="Lasts until the cervix is dilated to 5 or 6 cm"/>
<node CREATED="1588845468994" ID="ID_212407944" MODIFIED="1588845481590" TEXT="Goes faster when:">
<node CREATED="1588845482905" ID="ID_133911121" MODIFIED="1588845491023" TEXT="The cervix has moved forward and is very soft and thin"/>
<node CREATED="1588845492138" ID="ID_1905605745" MODIFIED="1588845500944" TEXT="The contractions are intense and close together"/>
<node CREATED="1588845516994" ID="ID_634152828" MODIFIED="1588845519552" TEXT="The baby is in the occiput anterior (OA) position"/>
<node CREATED="1588845534890" ID="ID_748236351" MODIFIED="1588845535768" TEXT="The baby&#x2019;s head has begun to move down into the pelvis"/>
</node>
</node>
<node CREATED="1588844848899" ID="ID_433883785" MODIFIED="1588919144178">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Active Phase</b>&#160;(or <b>Active Labor</b>) [11.179]
    </p>
  </body>
</html></richcontent>
<node CREATED="1588847770711" ID="ID_1357317988" MODIFIED="1589083060776" TEXT="3 - 7 hours"/>
<node CREATED="1588847519964" ID="ID_127360426" MODIFIED="1588847531690" TEXT="Cervix has dilated to about 6 cm and now begins to dilate faster"/>
<node CREATED="1588847543932" ID="ID_1457486827" MODIFIED="1588847550610" TEXT="Lasts until cervical dilation reaches about 8 cm"/>
<node CREATED="1588847578301" ID="ID_461928291" MODIFIED="1588847606696" TEXT="Contractions last &gt; 1 minute and come every 3 to 4 minutes."/>
<node CREATED="1588847621645" ID="ID_123017879" MODIFIED="1588847658873" TEXT="Contractions are very intense, and described as very painful, but manageable."/>
</node>
<node CREATED="1588921726945" ID="ID_1243923124" MODIFIED="1588922121558">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Transition Phase&#160;</b>&#160;[11.228]
    </p>
  </body>
</html></richcontent>
<node CREATED="1588922019147" ID="ID_1785699209" MODIFIED="1589083048324" TEXT="15 minutes - 2 hours"/>
<node CREATED="1588921761473" ID="ID_1455600998" MODIFIED="1588921786957" TEXT="Cervix dilates from about 8 or 9 cm to about 10"/>
<node CREATED="1588921787985" ID="ID_1023570896" MODIFIED="1588921788614" TEXT="Baby begins to descend."/>
<node CREATED="1588921805633" ID="ID_1772876555" MODIFIED="1588921807126" TEXT="The head moves from within the uterus through the cervix and down into the vagina"/>
<node CREATED="1588921818961" ID="ID_1180957745" MODIFIED="1588921831861" TEXT="Contractions at maximum intensity, each lasting 1 to 2 minutes and occurring very close together"/>
<node CREATED="1588921895298" ID="ID_1992461035" MODIFIED="1588921955815">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Pushing and bearing down</b>. &#8220;urge to push.&#8221;. Catching their breath, grunting, or holding their breath and straining
    </p>
  </body>
</html></richcontent>
<node COLOR="#006699" CREATED="1588921970907" ID="ID_1311295432" MODIFIED="1588921984916" TEXT="Involuntary reflex">
<font NAME="SansSerif" SIZE="10"/>
</node>
</node>
</node>
</node>
<node CREATED="1588844738522" ID="ID_552361574" MODIFIED="1588844938976">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ends when the cervix has dilated completely (to ~10 cm)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1588842571254" ID="ID_299212304" MODIFIED="1588842615751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>5)</b>&#160;The baby&#8217;s chin tucks onto the chest (<i>flexion</i>) and the head rotates.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588842640943" ID="ID_1797040092" MODIFIED="1588842656551">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>6)</b>&#160;The baby descends
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588842692127" ID="ID_113448320" MODIFIED="1588842720316" TEXT="Described in terms of &#x201c;station,&#x201d; which tells how far above or below the pregnant person&#x2019;s midpelvis the baby&#x2019;s head is"/>
<node CREATED="1588842722624" ID="ID_1876483446" MODIFIED="1588842742516" TEXT="Measured in cm"/>
<node CREATED="1588842745376" ID="ID_589205720" MODIFIED="1588842798436" TEXT="Ranges from -4cm to +4cm. A &#x201c;zero station&#x201d; means the baby&#x2019;s head is right at midpelvis. "/>
<node CREATED="1588842806168" ID="ID_1392147373" MODIFIED="1588842829765" TEXT="The more positive the number, the closer the baby&#x2019;s head is to the outside and to being born."/>
<node CREATED="1588923254943" ID="ID_1754589705" MODIFIED="1588926925964">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Second Stage</b>: Birthing [11.275]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1588923569930" ID="ID_900455605" MODIFIED="1588923583094" TEXT="15 minutes - 3 hours"/>
<node CREATED="1588923304375" ID="ID_584079902" MODIFIED="1588923339724">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Begins when the cervix is fully dilated and ends with the birth of the baby.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1588923340768" ID="ID_743798932" MODIFIED="1588923341644" TEXT="Baby rotates, descends through the vagina (birth canal) and is born"/>
<node CREATED="1588923410976" ID="ID_1669786653" MODIFIED="1588923435820" TEXT="She works very hard, bearing down - actively pushing, by holding their breath and straining or breathing out forcefully and vocally with the urge to push that comes several times in every contraction."/>
<node CREATED="1588923456913" ID="ID_1279335571" MODIFIED="1588923460709" TEXT="Phases">
<node CREATED="1588923462921" ID="ID_1530020902" MODIFIED="1588930230879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Resting</b>&#160;[11.281]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588923867205" ID="ID_275132455" MODIFIED="1588923872798" TEXT="10 -30 minutes"/>
<node CREATED="1588923648755" ID="ID_1929963116" MODIFIED="1588923661007" TEXT="Apparent pause in labor"/>
<node CREATED="1588923662499" ID="ID_1004963809" MODIFIED="1588923679623" TEXT="Not all people experience this phase"/>
<node CREATED="1588923722555" ID="ID_414142817" MODIFIED="1588923730360" TEXT="Baby&#x2019;s head has passed through the cervix into the birth canal.">
<node CREATED="1588923731867" ID="ID_642423803" MODIFIED="1588923746280" TEXT="Uterus is now deflated around just the body"/>
<node CREATED="1588923760500" ID="ID_766532966" MODIFIED="1588923776944" TEXT="Uterus needs time to tighten around the rest of the baby"/>
<node CREATED="1588923799140" ID="ID_464156046" MODIFIED="1588923815797" TEXT="No contractions"/>
</node>
</node>
<node CREATED="1588923471665" ID="ID_64620842" MODIFIED="1588930236880">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Descent</b>&#160;[11.308]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588925020935" ID="ID_700028053" MODIFIED="1588925044865" TEXT="10 minutes - 4 hours. Average is 1.5 hours"/>
<node CREATED="1588924333553" ID="ID_1349915697" MODIFIED="1588924348637" TEXT="Uterus resumes contracting strongly, so they feel an increasingly strong urge to push."/>
<node CREATED="1588924361585" ID="ID_724329636" MODIFIED="1588924367821" TEXT="Baby descends through the birth canal to the point where the top of the head is clearly visible at the vaginal outlet."/>
<node CREATED="1588924408050" ID="ID_324433181" MODIFIED="1588924422238" TEXT="Push and breathes lightly during contractions and rest between contractions.">
<node CREATED="1588924433682" ID="ID_1544513927" MODIFIED="1588924439615" TEXT="Pushing means...">
<node CREATED="1588924449642" ID="ID_1507802911" MODIFIED="1588924453135" TEXT="Takes in a breath and strains (bears down) for 5 to 6 seconds at a time"/>
<node CREATED="1588924466226" ID="ID_1655579761" MODIFIED="1588924466975" TEXT="While bearing down, they either hold their breath or let air out with a moan or bellow"/>
<node COLOR="#990000" CREATED="1588924599427" ID="ID_431513797" MODIFIED="1588924614572" TEXT="Epidurals remove the urge to push, so pushing must be directed"/>
</node>
</node>
<node CREATED="1588924739205" ID="ID_298319362" MODIFIED="1588924746625" TEXT="Pushing can be directed by caregiver"/>
<node CREATED="1588924801509" ID="ID_938280493" MODIFIED="1588924804642" TEXT="Perineum bulges with bearing down"/>
<node CREATED="1588924824982" ID="ID_1056435309" MODIFIED="1588924836082" TEXT="Labia part, and the tiny vaginal outlet gradually enlarges as the baby moves down"/>
<node CREATED="1588924847814" ID="ID_1623508188" MODIFIED="1588924851243" TEXT="The head becomes visible, though at first it looks more like a wrinkled walnut."/>
<node CREATED="1588924906921" ID="ID_569460985" MODIFIED="1588924916395" TEXT="May change positions">
<node CREATED="1588924937127" ID="ID_936214858" MODIFIED="1588924946787" TEXT="Semi-reclining"/>
<node CREATED="1588924947303" ID="ID_677114287" MODIFIED="1588924959899" TEXT="Lying flat on back "/>
<node CREATED="1588924960855" ID="ID_1743915031" MODIFIED="1588924965597" TEXT="Side lying"/>
<node CREATED="1588924966567" ID="ID_1583719216" MODIFIED="1588924975628" TEXT="On hands and knees"/>
<node CREATED="1588924976471" ID="ID_812938499" MODIFIED="1588924980572" TEXT="Squatting"/>
</node>
</node>
<node CREATED="1588923474465" ID="ID_951706975" MODIFIED="1588930250527">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Crowning and birth</b>&#160;[11.357]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588927145164" ID="ID_1748382314" MODIFIED="1588927150117" TEXT="Takes only a few contractions."/>
<node CREATED="1588927003418" ID="ID_971666094" MODIFIED="1588927034406" TEXT="Begins when the baby&#x2019;s head crowns - that is, remains visible at the vaginal opening even between contractions, no longer sneaking back between the bearing-down efforts"/>
<node CREATED="1588927036034" ID="ID_1603381156" MODIFIED="1588927047927" TEXT="Ends with baby being born"/>
<node CREATED="1588927066611" ID="ID_1338754321" MODIFIED="1588927084319" TEXT="Baby&#x2019;s head stretches the vagina and perineum, which may cause feelings of burning and stinging"/>
<node CREATED="1588927085004" ID="ID_1795629402" MODIFIED="1588927085872" TEXT="Because the vaginal and perineal tissues might tear at this time, protecting the perineum now becomes a major focus of the caregiver&#x2019;s role."/>
</node>
</node>
</node>
</node>
<node CREATED="1588927598616" ID="ID_6361106" MODIFIED="1588927619477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>7)</b>&#160;Birthing the Placenta
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588927628408" ID="ID_927794911" MODIFIED="1588927659741">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Third Stage</b>: Placenta [11.389]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588927745788" ID="ID_776358850" MODIFIED="1588927752194" TEXT="15 - 30 minutes"/>
<node CREATED="1588927674913" ID="ID_1657607568" MODIFIED="1588927682405" TEXT="Begins when the baby is born and ends after the placenta, or afterbirth, is born."/>
<node CREATED="1588927699795" ID="ID_187009483" MODIFIED="1588927702349" TEXT="Usually anticlimactic">
<node CREATED="1588927703753" ID="ID_545154389" MODIFIED="1588927720334" TEXT="Many people barely notice the few contractions and the emergence of the placenta"/>
</node>
<node CREATED="1588927737865" ID="ID_653482114" MODIFIED="1588927739182" TEXT="Others feel sharp cramps"/>
</node>
</node>
<node CREATED="1588928253918" ID="ID_1262631293" MODIFIED="1588928270851">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>8)</b>&#160;Recovery and Bonding
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588928275566" ID="ID_1312067084" MODIFIED="1588928304859">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Fourth Stage</b>&#160;[11.422]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588928485056" ID="ID_961353139" MODIFIED="1588928489289" TEXT="2 - 4 hours"/>
<node CREATED="1588928318831" ID="ID_1437212106" MODIFIED="1588928323003" TEXT="First few hours after birth, when the birthing person&#x2019;s condition stabilizes."/>
<node CREATED="1588928356159" ID="ID_316442861" MODIFIED="1588928374235" TEXT="If the birth has been without medications or interventions, the birthing person&#x2019;s own oxytocin, which began to surge during the baby&#x2019;s journey down the birth canal, is now at high levels;">
<node CREATED="1588928411679" ID="ID_173684852" MODIFIED="1588928412724" TEXT="Epidural analgesia reduces oxytocin and endorphin production, which may moderate the positive feelings."/>
</node>
<node CREATED="1588928384023" ID="ID_342759535" MODIFIED="1588928387268" TEXT="Endorphins are also flowing, and these combine to give the birthing person high spirits and feelings of love and gratefulness"/>
<node CREATED="1588928459576" ID="ID_901349880" MODIFIED="1588928471492" TEXT="The structure of the baby&#x2019;s heart changes so all the baby&#x2019;s blood is rerouted through the lungs to pick up oxygen and carry it wherever needed."/>
<node CREATED="1588928660274" ID="ID_1226235891" MODIFIED="1588928701271" TEXT="Oxytocin stimulates the uterus to contract"/>
<node CREATED="1588928713154" ID="ID_911865744" MODIFIED="1588928730374" TEXT="Baby&#x2019;s actions stimulate the pituitary gland to secrete prolactin, the key to both the production of breast milk and to altruistic behavior"/>
</node>
</node>
</node>
</node>
</node>
</map>
