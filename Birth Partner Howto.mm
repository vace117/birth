<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1588840602128" ID="ID_1296779189" MODIFIED="1588840607520" TEXT="Birth">
<node CREATED="1588841084537" ID="ID_1624039352" MODIFIED="1588930330004" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Prelabor </b>[10.85]
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="full-1"/>
<node CREATED="1588840818815" ID="ID_1272592285" MODIFIED="1588841156565" TEXT="Non-progressing contractions">
<node CREATED="1588843163117" ID="ID_782143587" MODIFIED="1588843176965" TEXT="Realize that a long prelabor, though challenging, is not a medical problem in itself."/>
<node COLOR="#990000" CREATED="1588843197109" ID="ID_1699819672" MODIFIED="1588929218665" TEXT="Time Contractions">
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#990000" CREATED="1588846787053" ID="ID_627719852" MODIFIED="1588846921519" TEXT="Use the app to measure Duration and Interval"/>
<node COLOR="#990000" CREATED="1588846827478" ID="ID_1521347787" MODIFIED="1588846921990">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Start</b>&#160;of one contraction to the <b>start</b>&#160;of the next.
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#990000" CREATED="1588846904094" ID="ID_1310909724" MODIFIED="1588846914342" TEXT=" Don&#x2019;t fall into the trap of timing from the end of one to the start of the next!"/>
</node>
<node CREATED="1588841193074" ID="ID_943690148" MODIFIED="1588843111935" TEXT="Remind that dilation of the cervix beyond ~2 cm is one of the last things to happen, after the cervix has moved into position, ripened, and effaced. The fact that the cervix is not yet opening does not mean there is no progress."/>
<node CREATED="1588841241898" ID="ID_1053669973" MODIFIED="1588841266094" TEXT="If discouraged with prolonged period of nonprogressing contractions, remind them of the six ways that labor progresses"/>
<node CREATED="1588843222277" ID="ID_1180936549" MODIFIED="1588843232114" TEXT="Encourage to eat when hungry and drink when thirsty."/>
<node CREATED="1588843247453" ID="ID_251374126" MODIFIED="1588843261529" TEXT="Do projects or activities together to help get your minds off the contractions."/>
</node>
<node CREATED="1588929000861" ID="ID_1066870211" MODIFIED="1588929177673" TEXT="Signs of Labor [10.40]">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1589083454247" ID="ID_1470224056" MODIFIED="1589083476790">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Menstrual-like cramping</b>&#160;that comes &amp; goes in waves
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1589083485959" ID="ID_354803350" MODIFIED="1589083495366">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Dull backache</b>&#160;down low&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1589083502159" ID="ID_345257442" MODIFIED="1589083511966">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Pressure</b>&#160;&#8211; a feeling that your baby has &#8216;dropped&#8217;&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1589083522848" ID="ID_751775793" MODIFIED="1589083537678">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Abdominal cramping, Uneasy Stomach, or Loose Bowels</b>&#160;&#8211; often occurs the 24-48 hours prior to the start of labour&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1589083550048" ID="ID_1843210203" MODIFIED="1589083560461">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Any fluid </b>leaking from your vagina
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1589083563256" ID="ID_1771175236" MODIFIED="1589083565256" TEXT=" Note TACO Acronym: Time, Amount, Colour, Odour"/>
</node>
<node CREATED="1589083580080" ID="ID_261801037" MODIFIED="1589083601135">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Loss of Mucous Plug or Bloody Show</b>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1588843693585" ID="ID_813923717" MODIFIED="1588928946021" TEXT="Call the Doctor when:">
<node COLOR="#990000" CREATED="1588843725898" ID="ID_1286812111" MODIFIED="1588928948061">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Signs of labor before 37 weeks (premature labor)
    </p>
  </body>
</html>
</richcontent>
</node>
<node COLOR="#990000" CREATED="1588843753289" ID="ID_1486886025" MODIFIED="1588928949253" TEXT="Leaking or a gush of fluid from the vagina [10.75]"/>
</node>
<node COLOR="#990000" CREATED="1588843869139" ID="ID_1082546983" MODIFIED="1588929247458" STYLE="bubble" TEXT="Go to the Hospital when: [11.116]">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#990000" CREATED="1588843894155" ID="ID_185375773" MODIFIED="1588844072445" TEXT="12 - 15 consecutive contractions, which...">
<node COLOR="#990000" CREATED="1588843949963" ID="ID_1770553273" MODIFIED="1588844079413" TEXT="Last at least 1 minute"/>
<node COLOR="#990000" CREATED="1588843951931" ID="ID_478852963" MODIFIED="1588844080157" TEXT="Are 4 to 5 minutes apart"/>
<node COLOR="#990000" CREATED="1588843972524" ID="ID_1427456141" MODIFIED="1588844080749" TEXT="Are strong enough they cannot be distracted from them"/>
<node COLOR="#990000" CREATED="1588843987588" ID="ID_1112619649" MODIFIED="1588844081413" TEXT="Are strong enough they must use a breathing, relaxation, or attention-focusing ritual to get through them"/>
<node COLOR="#990000" CREATED="1588845606867" ID="ID_123753395" MODIFIED="1588845685779" TEXT="4-1-1 Rule">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#990000" CREATED="1588845624923" ID="ID_998861843" MODIFIED="1588845691339" TEXT="Contractions are 4 minutes apart, 1 minute long, for at least 1 hour">
<node COLOR="#990000" CREATED="1589083264885" ID="ID_1561155734" MODIFIED="1589083272093" TEXT=" Do not take a bowel movement, unless in hospital (urge to push feels similar to needing to poop)"/>
</node>
</node>
</node>
<node CREATED="1588929073086" ID="ID_750708356" MODIFIED="1588929212763" TEXT="Supplies for Hospital [8.113]">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1588843285021" ID="ID_290725948" MODIFIED="1588843291438" TEXT="Slow-to-Start">
<node CREATED="1588843378406" ID="ID_1388351472" MODIFIED="1588843379539" TEXT="Be patient and confident. This labor will not go on forever, and your positive attitude will help keep their spirits up."/>
<node CREATED="1588843398934" ID="ID_1454643885" MODIFIED="1588843417667" TEXT="Remind that a long prelabor does not necessarily mean something is wrong with them or the baby. The cervix simply needs more time before it thins and begins opening. The two of you need to find ways to wait without worrying."/>
<node CREATED="1588843461439" ID="ID_21584799" MODIFIED="1588843462556" TEXT="Try not to become preoccupied with the labor, analyze, or overreact to every contraction. This will only make the labor seem longer."/>
<node CREATED="1588843480151" ID="ID_1919532035" MODIFIED="1588843480740" TEXT="Encourage the pregnant person to eat and drink high-carbohydrate, easily digested foods (for example, toast with jam, cereals, pancakes, pasta, fruit juice, coconut water, tea with sugar or honey, sorbet, or gelatin desserts)."/>
<node CREATED="1588843573088" ID="ID_739690970" MODIFIED="1588843600837" TEXT="Try positions described here: [13.177]"/>
</node>
</node>
<node CREATED="1588845836485" ID="ID_178170898" MODIFIED="1588930333635" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Early Labor </b>[11.102]
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="full-2"/>
<node CREATED="1588845894477" ID="ID_1468739994" MODIFIED="1588845905545" TEXT="Her response to early labor will probably not be that different from the response to prelabor. They are still likely to feel somewhat uncertain as the Positive Signs of labor become clear."/>
<node COLOR="#990000" CREATED="1588845965342" ID="ID_611572300" MODIFIED="1588846013711" TEXT="When the laboring person rushes labor mentally, they may want to go to the hospital too early, start using rhythmic breathing and other labor-coping measures before they are really needed, and speculate that the cervix is dilated much more than it really is.">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1588845989742" ID="ID_1103732760" MODIFIED="1588846014814">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Keep up the Distractions!
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1588846037399" ID="ID_505826126" MODIFIED="1588846068130" TEXT="Remain close by"/>
<node CREATED="1588846069254" ID="ID_1233863402" MODIFIED="1588846081059" TEXT="Supply food and drink"/>
<node CREATED="1588846083015" ID="ID_1938473027" MODIFIED="1588846108859" TEXT="Time 5 or 6 contractions, when the pattern seems to change"/>
<node CREATED="1588846109879" ID="ID_81736302" MODIFIED="1588846124693" TEXT="Help pass the time with pleasant and distracting activities"/>
</node>
<node CREATED="1588846154631" ID="ID_1652593544" MODIFIED="1588846455839">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      When labor patterns intensify, she will become preoccupied with the contractions [11.148]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588846239601" ID="ID_401439909" MODIFIED="1588846266068" TEXT="No longer able to walk or talk through them without pausing"/>
<node CREATED="1588846269224" ID="ID_403883945" MODIFIED="1588846282797" TEXT="Contractions &#x201c;stop them in their tracks.&#x201d; "/>
<node CREATED="1588846292977" ID="ID_720989752" MODIFIED="1588846322341" TEXT="From this point on, do not distract her. Instead...">
<node CREATED="1588846335130" ID="ID_1971785807" MODIFIED="1588846351973" TEXT="Give her your undivided attention throughout every contraction."/>
<node CREATED="1588846353009" ID="ID_964410984" MODIFIED="1588846362853" TEXT="Stop what you are doing and stop talking so you can focus."/>
<node CREATED="1588846364025" ID="ID_747697792" MODIFIED="1588926587449" TEXT="Do not ask questions during contractions.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1588846396770" ID="ID_942583685" MODIFIED="1588846531839" TEXT="Watch during the contractions and, if you notice tension, help the laboring person relax their entire body during each one [12.27]"/>
<node CREATED="1588846547683" ID="ID_768183874" MODIFIED="1588846571231" TEXT="Suggest using the planned ritual (&#x201c;relax, breathe, and focus&#x201d;)&#x2014;slow, rhythmic breathing while focusing on something pleasant or positive, such as letting go of tension with each exhale [12.123]"/>
<node CREATED="1588846602900" ID="ID_594362625" MODIFIED="1588846627721">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Encourage slow, rhythmic breathing through comforting touch and verbal encouragement during each contraction (<i>&#8220;That&#8217;s good&#8230; . Just like that.&#8221;</i>) and helpful comments when the contraction is over (<i>&#8220;You relaxed very well that time,&#8221;</i>&#160;or <i>&#8220;I noticed you tensed your shoulders with that contraction; with the next one, focus on keeping your shoulders relaxed.&#8221;</i>).
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1589083185325" ID="ID_1240742452" MODIFIED="1589083197089" TEXT="Physical Coping Help">
<node CREATED="1589083208037" ID="ID_251266115" MODIFIED="1589083209217" TEXT="Slow dancing and lifting the belly allows baby to rotate"/>
<node CREATED="1589083215741" ID="ID_1710720941" MODIFIED="1589083216449" TEXT="Do lunges during contractions"/>
<node CREATED="1589083222869" ID="ID_1524084259" MODIFIED="1589083223509" TEXT="Ask for exercise ball "/>
<node CREATED="1589083232229" ID="ID_1840042864" MODIFIED="1589083232786" TEXT="Ask for peanut ball"/>
</node>
</node>
</node>
</node>
<node CREATED="1588919073048" ID="ID_521835666" MODIFIED="1588930336646" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Active labor </b>[11.179]
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="full-3"/>
<node CREATED="1588919206706" ID="ID_345797842" MODIFIED="1588919209258" TEXT="Extraneous conversation becomes annoying.">
<node CREATED="1588919248810" ID="ID_1774128075" MODIFIED="1588919249862" TEXT="When a contraction ends, conversation, if any, is likely to consist of reviewing it with you and talking about what to do for the next one."/>
</node>
<node CREATED="1588919180697" ID="ID_1979318207" MODIFIED="1588919182534" TEXT="Maintain a rhythmic ritual - releasing tension, breathing, moving, or moaning during contractions"/>
<node CREATED="1588919332587" ID="ID_1976818924" MODIFIED="1588919404671" TEXT="[11.188] Benefits from a quiet room, freedom to move around in and out of bed, and as little disturbance or interruption as possible."/>
<node CREATED="1588919376307" ID="ID_168922536" MODIFIED="1588919381087" TEXT="May want to be held or stroked or, conversely, not touched at all."/>
<node COLOR="#990000" CREATED="1588919734766" ID="ID_1314769632" MODIFIED="1588919749841" TEXT="Make sure staff are aware of the birth plan, especially with regard to preferences for pain medications and other interventions."/>
<node CREATED="1588919779721" ID="ID_1888006370" MODIFIED="1588919870884">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Match their mood</b>. If serious or quiet, you be serious or quiet. Don&#8217;t try to jolly them out of this mood or distract them at this time.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588919806743" ID="ID_1233031609" MODIFIED="1588919820892">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Acknowledge feelings.</b>&#160;If the laboring person says, &#8220;I can&#8217;t do this,&#8221; you might reply, &#8220;This is rough. Let me help you more. Keep your rhythm.&#8221;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588920791808" ID="ID_742597983" MODIFIED="1588920803551" TEXT="Remind them that it&apos;s difficult, but not impossible"/>
</node>
<node CREATED="1588919844775" ID="ID_1950857306" MODIFIED="1588919853557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Offer liquid to drink after each contraction or two</b>. Don&#8217;t disturb the laboring person by asking what they would like to drink; just hold the beverage where it can be seen.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588919907944" ID="ID_1514597854" MODIFIED="1588919924285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Give the laboring person your undivided attention</b>&#160;throughout every contraction, even when their eyes are closed and you think it&#8217;s not needed.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588919951840" ID="ID_1999569574" MODIFIED="1588919964413">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Help with comfort measures</b>. Hold the laboring person and slow dance; rub their shoulders or press on their back; walk with them; stay beside the shower or bathtub or get in, too
    </p>
  </body>
</html>
</richcontent>
</node>
<node COLOR="#990000" CREATED="1588919974401" ID="ID_525263915" MODIFIED="1588920260619">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Support their &#8220;ritual&#8221;. </b>Help maintain a rhythm through each contraction.
    </p>
  </body>
</html>
</richcontent>
<node COLOR="#990000" CREATED="1588920045233" ID="ID_614171855" MODIFIED="1588920262291" TEXT="Relaxation [12.65]"/>
<node COLOR="#990000" CREATED="1588920065913" ID="ID_1009423651" MODIFIED="1588920263061" TEXT="Rhythmic Breathing and Moaning [12.123]"/>
</node>
<node CREATED="1588920089818" ID="ID_672678933" MODIFIED="1588920208255">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Rhythm is everything</b>. Key to coping in the dilation stage. If there is rhythm in whatever the laboring person does during contractions, they are coping.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588920211723" ID="ID_1936889142" MODIFIED="1588920228840" TEXT="(moaning, swaying, tapping, rocking, chanting, even silent self-talk)"/>
<node CREATED="1588920230075" ID="ID_1433448830" MODIFIED="1588920239191" TEXT="Or whatever they want you to do (holding them, stroking them, swaying together, talking, nodding your head, moaning together)"/>
<node CREATED="1588920311148" ID="ID_1186014309" MODIFIED="1588920339392" TEXT="If the laboring person loses rhythm and tenses, grimaces, writhes, clutches, or cries out, they need your help to regain the rhythm they had or to find a new one."/>
<node CREATED="1588920351204" ID="ID_462965900" MODIFIED="1588920364048" TEXT="If you have been in their rhythm, it&#x2019;s much easier to help them get it back. "/>
<node CREATED="1588920365604" ID="ID_2637788" MODIFIED="1588920366552" TEXT="At times, you might help by making eye contact or by rhythmically talking, stroking, or swaying along with laboring person">
<node COLOR="#990000" CREATED="1588920382940" ID="ID_1582378955" MODIFIED="1588921061539" TEXT="Take-Charge Routine [13.18]">
<node CREATED="1588920444669" ID="ID_61943703" MODIFIED="1588920484276">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Applicable when the laboring person:
    </p>
    <p>
      &#160;&#160;&#8226; Is unable to maintain a rhythmic ritual in breathing, moaning, or movements
    </p>
    <p>
      &#160;&#160;&#8226; Despairs, weeps, cries out, or says they cannot go on
    </p>
    <p>
      &#160;&#160;&#8226; Is very tense and cannot relax &#8226; Is in a great deal of pain
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1588921193764" ID="ID_1209380101" MODIFIED="1588921210888" TEXT="Take the contractions one at a time. Your job is to keep a rhythm during the contractions, and you&#x2019;re doing it. I&#x2019;ll help you."/>
<node CREATED="1588921098331" ID="ID_928766265" MODIFIED="1588921130159" TEXT="Don&#x2019;t be afraid to suggest something new, if they lose the rhythm and have real trouble getting back to it."/>
<node CREATED="1589083316094" ID="ID_1236162289" MODIFIED="1589083320492" TEXT="Physical Coping Help">
<node CREATED="1589083327606" ID="ID_928512300" MODIFIED="1589083328210" TEXT="Massage"/>
<node CREATED="1589083334230" ID="ID_1338497682" MODIFIED="1589083377563" TEXT="Counter pressure (SI joints and sacrum pressure with palm)"/>
<node CREATED="1589083345486" ID="ID_1217522952" MODIFIED="1589083387678" TEXT="Double Hip Squeeze"/>
<node CREATED="1589083397143" ID="ID_1163894921" MODIFIED="1589083397870" TEXT="Efflauge - light &quot;rain&quot; touch"/>
</node>
<node COLOR="#990000" CREATED="1588921302589" ID="ID_605733677" MODIFIED="1588921310485" TEXT="Interventions">
<node CREATED="1588921353381" ID="ID_1086488489" MODIFIED="1588921355268" TEXT="Tests">
<node CREATED="1588921357861" ID="ID_1986182914" MODIFIED="1588921446857">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ask the following:
    </p>
    <p>
      &#8226; What is the reason for the test?
    </p>
    <p>
      &#8226; What questions will it answer?
    </p>
    <p>
      &#8226; How is the test done?
    </p>
    <p>
      &#8226; How accurate or reliable are the results? What is the margin of error?
    </p>
    <p>
      &#8226; If the test detects a problem, what happens next (for example, further testing or immediate treatment)?
    </p>
    <p>
      &#8226; If the test does not detect a problem, what happens next (for example, a repeat test in a day or two, other tests, or no further concern about the problem)?
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1588921452566" ID="ID_1892575856" MODIFIED="1588921537571" TEXT="Procedures [14.20]">
<node CREATED="1588921461886" ID="ID_12643942" MODIFIED="1588921503164">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ask the following:
    </p>
    <p>
      &#160;&#160;&#8226; What is the problem and how serious is it?
    </p>
    <p>
      &#160;&#160;&#8226; How urgent is the need to begin treatment?
    </p>
    <p>
      &#160;&#160;&#8226; What is the treatment and how is it done?
    </p>
    <p>
      &#160;&#160;&#8226; How likely is it to solve the problem?
    </p>
    <p>
      &#160;&#160;&#8226; If the treatment fails, what are the next steps?
    </p>
    <p>
      &#160;&#160;&#8226; Are there any side effects to the treatment?
    </p>
    <p>
      &#160;&#160;&#8226; Are there any alternatives (including waiting, doing nothing, or other treatments)?
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1588921726945" ID="ID_1243923124" MODIFIED="1588930340752" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Transition Phase&#160;</b>&#160;[11.228]
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="full-4"/>
<node CREATED="1588922019147" ID="ID_1785699209" MODIFIED="1588922139183" TEXT="15 minutes - 2 hours"/>
<node CREATED="1588922154788" ID="ID_1681150742" MODIFIED="1588922161473" TEXT="Contractions unlikely to get worse"/>
<node COLOR="#990000" CREATED="1588922958692" ID="ID_1518197488" MODIFIED="1588923049346">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>If they feel &quot;Urge to Push&quot;,</b>&#160;summon help immediately! The caregiver will observe her behavior or check the cervix to determine whether pushing is appropriate.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588923072117" ID="ID_200840575" MODIFIED="1588923115177" TEXT="If the caregiver says it isn&#x2019;t time to push yet (because the cervix isn&#x2019;t fully dilated), help the laboring person avoid pushing or push with little grunts [11.230]"/>
</node>
<node COLOR="#000000" CREATED="1588922189725" ID="ID_275392974" MODIFIED="1588926525132" TEXT="Sensations [11.234]">
<node CREATED="1588922213941" ID="ID_1066067686" MODIFIED="1588922215050" TEXT="The frequency of contractions, combined with the sensations of the baby&#x2019;s head moving down, may cause the laboring person&#x2019;s legs, or even whole body, to tremble."/>
<node CREATED="1588922220581" ID="ID_1042194872" MODIFIED="1588922234772" TEXT="May feel nauseated and vomit; vomiting usually brings relief from the nausea."/>
<node CREATED="1588922242181" ID="ID_241997174" MODIFIED="1588922245085" TEXT="May feel pressure in the thighs or pelvis."/>
<node CREATED="1588922324886" ID="ID_1551965782" MODIFIED="1588922329875" TEXT="May weep or cry out, feeling they cannot handle any more"/>
<node CREATED="1588922342798" ID="ID_467842951" MODIFIED="1588922348674" TEXT="May feel overwhelmed and frustrated and react by saying, &#x201c;Don&#x2019;t touch me! My skin hurts!&#x201d; or &#x201c;I can&#x2019;t go on!&#x201d; or &#x201c;Stop doing that!&#x201d; or &#x201c;I want an epidural right now!&#x201d;"/>
</node>
<node CREATED="1588922378463" ID="ID_1458252055" MODIFIED="1588922544561" TEXT="High adrenaline"/>
<node COLOR="#990000" CREATED="1588922471695" ID="ID_606593657" MODIFIED="1588922488072" TEXT="Nurse of midwife will be much more involved"/>
<node CREATED="1588922508648" ID="ID_1031562395" MODIFIED="1588922520220" TEXT="Preparations for the infant start"/>
<node CREATED="1588922608057" ID="ID_629271477" MODIFIED="1588922624063">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Help the laboring person maintain or resume the <b>rhythmic ritual</b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588922637801" ID="ID_1837013994" MODIFIED="1588922660308">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Stop worrying about relaxation</b>&#160;during contractions. It's unrealistic.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588922675105" ID="ID_345619909" MODIFIED="1588922686230">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Stay calm</b>. Keep your touch firm and confident and your voice calm and encouraging. Maintain a confident expression on your face.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588922697690" ID="ID_1309653032" MODIFIED="1588922719495">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Stay close, </b>with my face near theirs.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588922735226" ID="ID_1786722685" MODIFIED="1588922762183">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>If panicky and afraid</b>, use the Take-Charge routine. This may be the most important way you can help.
    </p>
  </body>
</html>
</richcontent>
<node COLOR="#990000" CREATED="1588920382940" ID="ID_1735559946" MODIFIED="1588921061539" TEXT="Take-Charge Routine [13.18]"/>
</node>
<node CREATED="1588922793675" ID="ID_1236698032" MODIFIED="1588922805631">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Remind them</b>&#160;that this difficult phase is short and that the birthing stage is imminent.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588922806443" ID="ID_1685313283" MODIFIED="1588922818487">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Help </b>through one contraction at a time.
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1588922872628" ID="ID_92372554" MODIFIED="1588926561345">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Do not mention pain meds</b>. Instead, help to get through this phase without them, as tough as it may be.
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1588923254943" ID="ID_1754589705" MODIFIED="1588930343799" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Second (Birthing) Stage </b>[11.275]
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="full-5"/>
<node CREATED="1588923569930" ID="ID_900455605" MODIFIED="1588924139232" TEXT="15 minutes - 3 hours"/>
<node CREATED="1588923456913" ID="ID_1279335571" MODIFIED="1588924139233" TEXT="Phases">
<node CREATED="1588923462921" ID="ID_1530020902" MODIFIED="1588930283151">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Resting</b>&#160;[11.281]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588923867205" ID="ID_275132455" MODIFIED="1588924139233" TEXT="10 -30 minutes"/>
<node CREATED="1588923648755" ID="ID_1929963116" MODIFIED="1588924139233" TEXT="Apparent pause in labor"/>
<node CREATED="1588924188224" ID="ID_377825162" MODIFIED="1588924206732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Be patient.</b>&#160;Don&#8217;t rush the birthing person through it or make them push too soon.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588924222464" ID="ID_1353064860" MODIFIED="1588924238127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      If caregiver wants the birthing person to push without a contraction or an urge to push, <b>ask whether it can wait </b>until they feel the urge.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588924257728" ID="ID_70247757" MODIFIED="1588924269669">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Match the birthing person&#8217;s mood</b>. As they leave the emotions of transition behind, you do the same.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1588923471665" ID="ID_64620842" MODIFIED="1588930291003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Descent</b>&#160;[11.308]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588925020935" ID="ID_700028053" MODIFIED="1588925056203" TEXT="10 minutes - 4 hours. Average is 1.5 hours"/>
<node CREATED="1588924651758" ID="ID_1199613089" MODIFIED="1588926547479" TEXT="Rhythmic ritual is no longer possible">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1588925099640" ID="ID_1360685962" MODIFIED="1588925124933" TEXT="Adrenaline rush, renewed strength and determination"/>
<node CREATED="1588925151129" ID="ID_1971369205" MODIFIED="1588925160109" TEXT="This is the active, working hard phase"/>
<node COLOR="#990000" CREATED="1588925232073" ID="ID_431242051" MODIFIED="1588925244346" TEXT="Alarmed by the pressure in the vagina, they may instinctively &#x201c;hold back,&#x201d; that is, tense the pelvic floor against the baby&#x2019;s downward movement.">
<node CREATED="1588925245954" ID="ID_1400375800" MODIFIED="1588925298407" TEXT="Let go of the tension in the vagina, and pushing will feel much better"/>
</node>
<node CREATED="1588925285962" ID="ID_1037350955" MODIFIED="1588925364479">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Push like you are <b>puking in reverse</b>. (Down instead of up)
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1588926147586" ID="ID_507049016" MODIFIED="1588926508462" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Praise them </b>on how well they are doing - after every contraction
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#000000" CREATED="1588926195467" ID="ID_1089254294" MODIFIED="1588926531911">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Do not tell </b>them to push harder; you only make them feel inadequate. Instead, offer encouragement like, &#8220;That&#8217;s the way!&quot;
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1588926260955" ID="ID_1431502456" MODIFIED="1588926284321" TEXT="Help the birthing person get in and out of positions for pushing [12.279]"/>
<node CREATED="1588925391915" ID="ID_1767200776" MODIFIED="1588925403680" TEXT="If the baby&#x2019;s descent is extremely rapid, the birthing person may feel shocked and frightened by the intensity of the pain and the total lack of control over their body"/>
<node CREATED="1588925424651" ID="ID_1438228216" MODIFIED="1588925433927" TEXT="If the baby&#x2019;s descent is very slow, the birthing person may become discouraged.">
<node CREATED="1588925435515" ID="ID_597154354" MODIFIED="1588925436544" TEXT="It may be that the baby&#x2019;s head is molding gradually or rotating to fit through the pelvis."/>
<node CREATED="1588925453781" ID="ID_1061567201" MODIFIED="1588927230261" STYLE="bubble" TEXT="Needs much encouragement and reassurance">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1588926425141" ID="ID_829580081" MODIFIED="1588926441265">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Remind them to relax the perineum</b>: &#8220;Let go,&#8221; &#8220;Relax your bottom,&#8221; &#8220;Let the baby out.&#8221;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588926454853" ID="ID_1949913213" MODIFIED="1588926469450">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Request warm compresses </b>to be placed on the perineum.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1588926607262" ID="ID_1107798672" MODIFIED="1588926638932" TEXT="Remind that the baby is almost here!"/>
</node>
<node CREATED="1588923474465" ID="ID_951706975" MODIFIED="1588930298552">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Crowning and birth</b>&#160;[11.357]
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1588927158972" ID="ID_1400951824" MODIFIED="1588927159656" TEXT="Takes only a few contractions. "/>
<node CREATED="1588927253630" ID="ID_962277720" MODIFIED="1588927256401" TEXT="Painful"/>
<node COLOR="#990000" CREATED="1588927203797" ID="ID_1622711685" MODIFIED="1588930572433">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>To prevent the vagina or perineum from tearing</b>, the birthing person should pay attention to this feeling and listen to the caregiver, who will tell them to stop pushing in order to ease the baby out. <b>They should not push hard.</b>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1588927397166" ID="ID_1088011020" MODIFIED="1588927397715" TEXT="Stay close by."/>
<node CREATED="1588927408222" ID="ID_1542584556" MODIFIED="1588927431714" TEXT="Help with their position by holding a leg, lifting their shoulders for pushing, or letting them lean on you in a squatting position [12.220]"/>
<node COLOR="#990000" CREATED="1588927487031" ID="ID_1351271515" MODIFIED="1588930569592">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      If the caregiver tells the birthing person to stop pushing so as not to injure their body or the baby with too rapid a delivery, the birthing person may find it difficult to comply. <b>Help them avoid pushing </b>by getting them to follow directions
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1588927628408" ID="ID_927794911" MODIFIED="1588930346414" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Third Stage</b>: Placenta [11.389]
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="full-6"/>
<node CREATED="1588927745788" ID="ID_776358850" MODIFIED="1588927962626" STYLE="bubble" TEXT="15 - 30 minutes"/>
</node>
<node CREATED="1588928275566" ID="ID_1312067084" MODIFIED="1588930349713" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Fourth Stage</b>&#160;[11.422]
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="full-7"/>
<node CREATED="1588928485056" ID="ID_961353139" MODIFIED="1588928571210" TEXT="2 - 4 hours"/>
<node CREATED="1588928318831" ID="ID_1437212106" MODIFIED="1588928571210" TEXT="First few hours after birth, when the birthing person&#x2019;s condition stabilizes."/>
<node CREATED="1588928356159" ID="ID_316442861" MODIFIED="1588928571210" TEXT="If the birth has been without medications or interventions, the birthing person&#x2019;s own oxytocin, which began to surge during the baby&#x2019;s journey down the birth canal, is now at high levels;"/>
<node CREATED="1588928384023" ID="ID_342759535" MODIFIED="1588928571210" TEXT="Endorphins are also flowing, and these combine to give the birthing person high spirits and feelings of love and gratefulness"/>
<node CREATED="1588928607761" ID="ID_1263961891" MODIFIED="1588928620654">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Keep birthing parent and baby together skin to skin</b>&#160;in the first hours after birth
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
